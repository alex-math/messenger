import datetime

import requests
from PyQt5 import QtWidgets, QtCore
import clientui


class Window(QtWidgets.QDialog, clientui.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton.pressed.connect(self.send_message)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.get_messages)
        self.timer.start(1000)
        self.after = 0
        # self.url = 'http://9d874ddff635.ngrok.io'
        self.url = 'http://127.0.0.1:5000'

    def send_message(self):
        name = self.lineEdit.text()
        text = self.textEdit.toPlainText()

        try:
            response = requests.post(f'{self.url}/send', json={
                'name': name,
                'text': text
            })
        except:
            self.textBrowser.append('Сервер недоступен')
            self.textBrowser.append('')
            return

        if response.status_code != 200:
            self.textBrowser.append('Никнейм или сообщение не заполнены')
            self.textBrowser.append('')
            return

        self.textEdit.clear()

    def get_messages(self):
        try:
            response = requests.get(f'{self.url}/messages',
                                    params={'after': self.after})
        except:
            return

        response_data = response.json()

        for message in response_data['messages']:
            self.print_message(message)
            self.after = message['time']

    def print_message(self, message):
        beauty_time = datetime.datetime.fromtimestamp(message['time']) \
            .strftime('%Y/%m/%d %H:%M')
        self.textBrowser.append(beauty_time + ' ' + message['name'])
        self.textBrowser.append(message['text'])
        self.textBrowser.append('')


app = QtWidgets.QApplication([])
window = Window()
window.show()
app.exec_()
